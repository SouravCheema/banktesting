package bankingApp;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Currency {
	private String name;
	private double rate;
	
	
	public Currency(String currencyCode, double rate) {
		this.name = currencyCode;
		this.rate = rate;
	}

	/** 
	 * Convert an amount of this Currency to its value in USD
	 * (As mentioned in the documentation of the Currency constructor)
	 * 
	 * @param amount An amount of cash of this currency.
	 * @return The value of amount in the "universal currency" (USD)
	 */
	public double valueInUSD(double amount) {
		// @TODO:  Fill in the code for this
		var newAmount = 0.0;
		if(this.name == "CAD") {
			newAmount = amount * 0.75;
			return newAmount;
		}
		else if(this.name == "EUR") {
			newAmount = amount * 1.14;
			return newAmount;
		}
		else if(this.name == "HKD") {
			newAmount = amount * 0.13;
			return newAmount;
		}
		else {
			amount = 0.0;
		}
		
		// Round your final answer to 2 decimal points. See round() function.
		var convertedAmount = round(newAmount,2);
		return convertedAmount;
	}

	/** 
	 * Get the name of this Currency.
	 * @return name of Currency
	 */
	public String getName() {
		// @TODO:  Fill in the code for this
		return this.name;
	}
	
	/** 
	 * Get the rate of this Currency.
	 * 
	 * @return rate of this Currency
	 */
	public double getRate() {
		// @TODO:  Fill in the code for this
		return this.rate;
	}
	
	/**
	 *  Set the rate of this currency.
	 * 
	 * @param rate New rate for this Currency
	 */
	public void setRate(Double rate) {
		// @TODO:  Fill in the code for this
		this.rate = rate;
	}
	
	/** 
	 * Convert an amount from another Currency to an amount in this Currency
	 * Do not convert according to real-life 
	 * Example - do NOT do this:  1 CAD = 2.84 BRL, therefore 10 CAD = 28.34 BRL
	 * 
	 * Instead, you should perform your calculations relative to the universal USD currency.
	 * 
	 * Example: Convert 10 CAD to BRL.
	 *   - 1 CAD = 0.75 USD
	 *   - 1 BRL = 0.27 USD
	 *   - So:   		10 CAD = 7.5 USD
	 *   - And: 		7.5 USD = 27.78 BRL
	 *   - Therefore:   10 CAD = 27.78 BRL
	 * @param amount Amount of the other Currency
	 * @param othercurrency The other Currency
	*/
	public double valueInThisCurrency(double amount, Currency othercurrency) {
		// @TODO:  Fill in the code for this
		double newCurrency = amount * othercurrency.rate;
		System.out.println(newCurrency);
		// Round all final results to 2 decimal points. See round() function.
		double amount1 = round(newCurrency,2);
		System.out.println(amount1);
		return round(newCurrency,2);
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
