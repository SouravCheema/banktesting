package bankingApp;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bankingApp.AccountDoesNotExistException;
import bankingApp.AccountExistsException;
import bankingApp.Bank;
import bankingApp.Currency;

public class BankTest {
	protected Currency CAD;
	protected Currency HKD;
	protected Bank RBC;
	protected Bank TD;
	protected Bank HSBC;
	
	
	@Before
	public void setUp() throws Exception {
		
		// setup some test currencies
		this.HKD = new Currency("HKD", 0.13);
		this.CAD = new Currency("CAD", 0.75);
		
		// setup test banks
		this.RBC = new Bank("Royal Bank of Canada", CAD);
		this.TD = new Bank("TD Bank", CAD);
		this.HSBC = new Bank("Hong Kong Shanghai Banking Corporation", HKD);
		
		// add sample customers to the banks
		
		
		// HINT:  uncomment these lines AFTER you test the openAccount() function
		// You can quickly uncomment / comment by highlighting the lines of code and pressing 
		// CTRL + / on your keyboard  (or CMD + / for Macs)
		
		this.RBC.openAccount("Marcos");
		this.RBC.openAccount("Albert");
		this.TD.openAccount("Jigesha");
		this.HSBC.openAccount("Pritesh");
	}

	@Test
	public void testGetName() {
		String name = RBC.getName();
		assertEquals("Royal Bank of Canada",name);
	}

	@Test
	public void testGetCurrency() {
		String currency = RBC.getCurrency().getName();
		assertEquals("CAD",currency);
	}


	@Test//(expected = AccountExistsException.class)
	public void testOpenAccount() throws AccountExistsException, AccountDoesNotExistException {
		// If the function throws an exception, you should also test
		// that the exception gets called properly.
		 
		  boolean thrown = false;

		  try {
			this.RBC.openAccount("Marcos");
			this.RBC.openAccount("Marcos");
		  } catch (AccountExistsException e) {
		    thrown = true;
		    assertEquals("AccountExistsException",e.getMessage());
		  } catch (Exception e) {
		    assertEquals("AccounDoesNotExistException",e.getMessage());
		  }
		 
		// See the example in class notes for testing exceptions.
		
	}

	@Test
	public void testDeposit() throws AccountDoesNotExistException {
		// If the function throws an exception, you should also test
		// that the exception gets called properly.
		
		// See the example in class notes for testing exceptions.
		try {
			Money money = new Money(100,CAD);
			this.RBC.deposit("Marcos", money);
		} catch(AccountDoesNotExistException e) {
			assertEquals("AccountDoesNotExistException",e.getMessage());
		}

	}

	@Test
	public void testWithdraw() throws AccountDoesNotExistException {
		// If the function throws an exception, you should also test
		// that the exception gets called properly.
		
		// See the example in class notes for testing exceptions.
		try {
			Money money = new Money(100,CAD);
			this.RBC.withdraw("Marcos", money);
		} catch(AccountDoesNotExistException e) {
			assertEquals("AccountDoesNotExistException",e.getMessage());
		}
		//fail("Write test case here");
	}
	
	@Test
	public void testGetBalance() throws AccountDoesNotExistException {
		// If the function throws an exception, you should also test
		// that the exception gets called properly.
		
		// See the example in class notes for testing exceptions.
		try {
			Money money = new Money(100,CAD);
			this.RBC.getBalance("Marcos");
		} catch(AccountDoesNotExistException e) {
			assertEquals("AccountDoesNotExistException",e.getMessage());
		}
		//fail("Write test case here");
	}
	
	@Test
	public void testTransfer() throws AccountDoesNotExistException {
		// Note: You should test both types of transfers:
		// 1. Transfer from account to account
		try {
			Money money = new Money(100,CAD);
			this.RBC.transfer("Marcos", "Jigisha", money);
		} catch(AccountDoesNotExistException e) {
			assertEquals("AccountDoesNotExistException",e.getMessage());
		}
		// 2. Transfer between banks
		try {
			Money money = new Money(100,CAD);
			this.RBC.transfer("Marcos", RBC, "Jigisha", money);
		} catch(AccountDoesNotExistException e) {
			assertEquals("AccountDoesNotExistException",e.getMessage());
		}
		// See the Bank.java file for more details on Transfers
		
	}
	
}
