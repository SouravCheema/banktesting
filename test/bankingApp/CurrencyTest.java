package bankingApp;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bankingApp.Currency;

public class CurrencyTest {
	
	/* Example currencies: 
	 * 	CAD = Canadian dollar
	 * 	EUR = Euros
	 * 	GBP = Great British Pounds
	 * 	HKD = Hong Kong Dollars
	 */
	public Currency CAD, EUR, GBP, HKD;
	
	@Before
	public void setUp() throws Exception {
		// Setup some test currencies to use in the below test cases
		CAD = new Currency("CAD", 0.75);
		EUR = new Currency("EUR", 1.14);
		HKD = new Currency("HKD", 0.13);
	}

	@Test
	public void testGetName() {
		// Write the test case for testing the getName() function
		String newCurrencyName = CAD.getName();
		assertEquals("CAD",newCurrencyName);
	}
	
	@Test
	public void testGetRate() {
		// @TODO: Write the test case for testing the getRate() function
		double newRate = CAD.getRate();
		assertEquals(0.75,newRate,2);
	}
	
	@Test
	public void testSetRate() {
		// @TODO: Write the test case for testing the setRate() function
		
		// For this function, you should do:
		// 1. Assert that the oldRate is correct
		double oldRate = CAD.getRate();
		assertEquals(0.75,oldRate,0.001);
		
		// 2. Change the rate
		CAD.setRate(0.66);
		// 3. Assert that the newRate is correct
		double newrate = CAD.getRate();
		assertEquals(0.66,newrate,0.001);
		// You will end up with 2 assert() statements in this function.
		
	}
	
	@Test
	public void testValueInUSD() {
		// @TODO: Write the test case for testing the valueInUSD() function
		double convertedAmount = CAD.valueInUSD(100);
		assertEquals(75,convertedAmount,0.001);
		
	}
	
	@Test
	public void testValueInThisCurrency() {
		// @TODO: Write the test case for testing the valueInThisCurrency() function
		// For this function, you should:
		// 1. Assert the value of the "other" currency
		double otherCurrency = CAD.valueInThisCurrency(100, EUR);
		assertEquals(114,otherCurrency,0.001);
		// 2. Get the value in "this" currency
		double currentCurrency = CAD.getRate();
		// 3. Assert that the value in "this" currency is correct
		assertEquals(0.75,currentCurrency,0.001);
		// You will end up with 2 assert() statements in this function.
	}

}
