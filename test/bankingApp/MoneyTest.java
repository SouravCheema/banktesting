package bankingApp;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bankingApp.Currency;
import bankingApp.Money;

public class MoneyTest {
	protected Currency CAD, HKD, NOK, EUR;
	protected Money CAD100, EUR10, CAD200, EUR20, CAD0, EUR0, CADnegative100;
	
	@Before
	public void setUp() throws Exception {
		// setup sample currencies
		CAD = new Currency("CAD", 0.75);
		HKD = new Currency("HKD", 0.13);
		EUR = new Currency("EUR", 1.14);
		
		// setup sample money amounts
		CAD100 = new Money(100, CAD);
		
		EUR10 = new Money(10, EUR);
		CAD200 = new Money(200, CAD);
		EUR20 = new Money(20, EUR);
		CAD0 = new Money(0, CAD);
		EUR0 = new Money(0, EUR);
		CADnegative100 = new Money(-100, CAD);
	}

	@Test
	public void testGetAmount() {
		double amount = CAD100.getAmount();
		assertEquals(100,amount,0.001);
	}

	@Test
	public void testGetCurrency() {
		//System.out.println(CAD100.getCurrency().getName());
		assertEquals("CAD",CAD100.getCurrency().getName());
	}

	@Test
	public void testToString() {
		String name = CAD100.toString();
		assertEquals("100.0"+ "CAD",name);
	}

	@Test
	public void testGetUniversalValue() {
		double value = CAD100.getUniversalValue();
		assertEquals(75,value,0.001);
	}

	@Test
	public void testEqualsMoney() {
		boolean value = CAD100.equals(EUR10);
		assertEquals(false,value);
	}

	@Test
	public void testAdd() {
		assertEquals(21.4,EUR10.add(EUR10).getAmount(),0.001);
	}

	@Test
	public void testSubtract() {
		assertEquals(-2.8,EUR20.subtract(EUR20).getAmount(),0.001);
	}

	@Test
	public void testIsZero() {
		assertEquals(true,CAD0.isZero());
	}

	@Test
	public void testNegate() {
		assertEquals(-100,CAD100.negate().getAmount(),0.001);
	}

	@Test
	public void testCompareTo() {
		assertEquals(0,CAD100.compareTo(CAD100));
	}
}
